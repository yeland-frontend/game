import React from "react";
import NewGame from "./NewGame";
import Answer from "./Answer";

class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      gameId: null,
      isNew: false
    };
    this.setUrl = this.setUrl.bind(this);
  }

  render() {
    return (
      <form>
        <NewGame setUrl={this.setUrl} />
        <Answer gameId={this.state.gameId} isNew={this.state.isNew}/>
      </form>
    )
  };

  setUrl(location, isNew) {
    this.setState({
      gameId: location.split("").reverse()[0],
      isNew: isNew
    });
  }
}

export default App;