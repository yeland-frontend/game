import React from "react";

class NewGame extends React.Component{
  constructor(props, context) {
    super(props, context);
    this.createGame = this.createGame.bind(this);
  }

  render() {
    return (
      <p onClick={this.createGame}>New Game</p>
    )
  }

  createGame() {
    fetch('http://localhost:8080/api/games', {method: 'POST'}).then(response =>
      this.props.setUrl(response.headers.get("location"), true)
    )
  }
}

export default NewGame;