import React from "react";

class Answer extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      numbers: "",
      hint: "",
      isCorrect: false,
      resultList: []
    };
    this.guess = this.guess.bind(this);
    this.setNumbers = this.setNumbers.bind(this);
    this.getAnswer = this.getAnswer.bind(this);
  }

  render() {
    if (this.props.isNew) {
      return (
        <div>
          <fieldset>
            <legend>Input Numbers</legend>
            <input className="number" type="text" placeholder="0-9"/>
            <input className="number" type="text" placeholder="0-9"/>
            <input className="number" type="text" placeholder="0-9"/>
            <input className="number" type="text" placeholder="0-9"/>
          </fieldset>
          <button id="submit" onClick={this.guess} type="reset">Good Luck</button>
          <ul>{this.state.resultList.map(result => <li>{result}</li>)}</ul>
        </div>
      )
    }
    return (
      <div/>
    )
  }

  guess() {
    const string = this.setNumbers();
    this.getAnswer(string);
  }

  setNumbers() {
    const inputs = document.getElementsByClassName("number");
    const numberString = inputs[0].value + inputs[1].value + inputs[2].value + inputs[3].value;
    this.setState({
      numbers: numberString
    });
    return numberString;
  }

  getAnswer(numbers) {
    const url = 'http://localhost:8080/api/games/' + this.props.gameId;
    const request = "{\"content\": \"" + numbers + "\"}";
    fetch(url, {method: 'POST', body: request})
      .then(response => response.json())
      .then(response => {
        this.setState({
          hint: response.hint,
          isCorrect: response.correct,
          resultList: [...this.state.resultList, `${numbers}: ${response.hint}`]
        });
        this.setMode(response.correct);
      });
  }

  setMode(isCorrect) {
    const inputs = document.getElementsByClassName("number");
    let isDisabled;
    if (isCorrect) {
      isDisabled = true;
    } else {
      isDisabled = false;
    }
    for(let item of inputs) {
      item.disabled = isDisabled;
    }
    document.getElementById("submit").disabled = isDisabled;
  }
}

export default Answer;